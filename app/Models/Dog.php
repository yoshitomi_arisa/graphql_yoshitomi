<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dog extends Model
{
    /**
     * 飼い主
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function human()
    {
        return $this->belongsTo('App\Models\Human');
    }

    /**
     * 嫌いな猫
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function enemies()
    {
        return $this->belongsToMany('App\Models\Cat', 'enemies');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Human extends Model
{

    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cats()
    {
        return $this->hasMany('App\Models\Cat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dogs()
    {
        return $this->hasMany('App\Models\Dog');
    }
}

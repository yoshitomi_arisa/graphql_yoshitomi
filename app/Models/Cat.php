<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    /**
     * 飼い主
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function human()
    {
        return $this->belongsTo('App\Models\Human');
    }

    /**
     * 嫌いな犬
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function enemies()
    {
        return $this->belongsToMany('App\Models\Dog', 'enemies');
    }
}

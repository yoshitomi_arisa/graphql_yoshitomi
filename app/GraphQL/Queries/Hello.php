<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Hello
{
    public function resolve($rootValue, array $args): string
    {
        return "Hello, {$args['name']}!";
    }
}

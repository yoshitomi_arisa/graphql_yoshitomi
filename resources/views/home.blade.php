<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>
</head>
<body>


<script>
    $.ajax({
        url: '{{ url('graphql') }}',
        query: 'query{human(id: 5){name\ndogs{name}}}'
    })
        .then(
            data => console.log(data),
            error => console.log(error.message),
        );
</script>
</body>
</html>

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HumanControllerTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->json('GET', '/api/humans');
        $response->assertStatus(200);
    }
}

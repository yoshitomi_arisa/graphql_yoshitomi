<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('humans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('cats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('human_id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('dogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('human_id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('enemies', function (Blueprint $table) {
            $table->unsignedBigInteger('cat_id');
            $table->unsignedBigInteger('dog_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('humans');
        Schema::dropIfExists('cats');
        Schema::dropIfExists('dogs');
        Schema::dropIfExists('enemies');
    }
}

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Human;
use App\Models\Cat;
use App\Models\Dog;
use Faker\Generator as Faker;

$factory->define(Human::class, function (Faker $faker) {
    return [
        'name' => $faker->userName
    ];
});

$factory->define(Cat::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName
    ];
});

$factory->define(Dog::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName
    ];
});
<?php

use Illuminate\Database\Seeder;
use App\Models\Cat;
use App\Models\Dog;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Human::class, 30)
            ->create()
            ->each(function ($human) {
                $human->cats()->saveMany(factory(Cat::class, rand(0, 3))->make());
                $human->dogs()->saveMany(factory(Dog::class, rand(0, 3))->make());
            });

        $cats = Cat::all();
        $dogs = Dog::all();

        foreach ($cats as $cat) {
            $cat->enemies()->saveMany($dogs->take(rand(0, 3)));
        }

    }
}
